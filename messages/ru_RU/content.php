<?php
return [
    'ID'              => 'ID',
    'Title'           => 'Title',
    'Name'            => 'Наименование',
    'Description'     => 'Описание',
    'Text'            => 'Описание',
    'Short Text'      => 'Краткое описание',
    'Image ID'        => 'Изображение',
];
