<?php
namespace emilasp\content\frontend;

use emilasp\core\CoreModule;
use emilasp\settings\behaviors\SettingsBehavior;
use emilasp\settings\models\Setting;
use yii\helpers\ArrayHelper;

/**
 * Class ContentModule
 * @package emilasp\content\frontend
 */
class ContentModule extends CoreModule
{
    public $menu;
    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'setting' => [
                'class'    => SettingsBehavior::className(),
                'meta'     => [
                    'name' => 'Статьи',
                    'type' => Setting::TYPE_MODULE,
                ],
                'settings' => [
                    /*[
                        'code'        => 'site_enabled',
                        'name'        => 'Сайт включён',
                        'description' => 'Включение/выключение сайта(заглушка)',
                        'default'     => 1,
                        'data'      => [
                            Setting::DEFAULT_SELECT_YES => 'Да',
                            Setting::DEFAULT_SELECT_NO  => 'Нет',
                        ],
                    ]
                    [
                        'code'    => 'index_page',
                        'name'    => 'Главная страница сайта',
                        'description' => 'ID страницы которая будет показываться на главной',
                        'default' => 1,
                    ],*/
                ],
            ],
        ], parent::behaviors());
    }
}
